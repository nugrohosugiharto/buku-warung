<?php require_once('component-header.php'); ?>
<main>
    <div class="container">
        <form action="">
            <div class="row">
                <section id="main-section" class="col-12 col-lg-9">
                    <div class="inner-wrap repeater">
                        <div class="inner-section first">
                            <div class="row">
                                <div class="col-12 col-lg-6">
                                    <div class="logo-upload">
                                        <div id="select-image">
                                            <label for="upload-logo">
                                                
                                                <div class="inner">
                                                    <img src="assets/images/ilustrasi-upload.png" alt="Upload" class="ilustrasi">
                                                    
                                                    <span>Klik untuk upload logo anda disini</span>
                                                </div>

                                                <input type="file" name="upload-logo" id="upload-logo" accept="image/*">

                                            </label>
                                        </div>
                                        <p>Upload Logo Company</p>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="invoice-info">
                                        <h1 class="title-invoice">INVOICE</h1>
                                        <div class="form-group">
                                            <div class="form-inner">
                                                <input type="text" class="form-input" value="#" placeholder="#001" id="invoice-id" name="invoice-id">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="invoice-datecreated">Tanggal Pembuatan</label>
                                            <div class="form-inner">
                                                <i class="icon-calendar"></i>
                                                <input type="text" class="form-input datepicker" name="invoice-datecreated" id="invoice-datecreated">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!--.inner-section-->

                        <div class="inner-section" style="padding-bottom: 20px;">
                            <div class="row">
                                <div class="col-12 col-lg-6">
                                    <div class="invoice-form form-sender">
                                        <h3 class="title-section">Invoice Pengirim</h3>

                                        <div class="form-group">
                                            <input type="text" class="form-input" id="sender-nama" name="sender-nama" placeholder="Nama">
                                        </div>

                                        <div class="form-group">
                                            <input type="email" class="form-input" id="sender-email" name="sender-email" placeholder="Email">
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-input" id="sender-npwp" name="sender-npwp" placeholder="NPWP (Opsional)">
                                        </div>

                                        <div class="form-group">
                                            <textarea name="sender-alamat" id="sender-alamat" name="sender-alamat" class="form-input" rows="3" placeholder="Alamat"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="invoice-form form-receiver">
                                        <h3 class="title-section">Invoice Penerima</h3>

                                        <div class="form-group">
                                            <input type="text" class="form-input" id="reveiver-nama" name="reveiver-nama" placeholder="Nama">
                                        </div>

                                        <div class="form-group">
                                            <input type="email" class="form-input" id="reveiver-email" name="reveiver-email" placeholder="Email">
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-input" id="reveiver-npwp" name="reveiver-npwp" placeholder="NPWP (Opsional)">
                                        </div>

                                        <div class="form-group">
                                            <textarea name="sender-alamat" id="reveiver-alamat" name="reveiver-alamat" class="form-input" rows="3" placeholder="Alamat"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!--.inner-section-->

                        <div class="inner-section" style="padding-bottom: 20px;margin-bottom: 20px;">
                            <div class="row">
                                <div class="col-12">
                                    <table class="table table-invoice table-nobordered">
                                        <thead>
                                            <tr>
                                                <th class="deskripsi">Deskripsi</th>
                                                <th class="item">Item</th>
                                                <th class="harga">Harga</th>
                                                <th class="total">Total</th>
                                                <th class="aksi">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody data-repeater-list="table-item">
                                            <tr data-repeater-item>
                                                <td class="deskripsi" data-title="Deskripsi">
                                                    <input type="text" class="form-input" placeholder="Nama item">
                                                    <input type="text" class="form-input" placeholder="Keterangan">
                                                </td>
                                                <td class="item" data-title="Item">
                                                    <input type="number" name="" id="" class="form-input" name="item-qty" id="item-qty" placeholder="Item" min="1" value="1">
                                                </td>
                                                <td class="harga" data-title="Harga">
                                                    <input type="text" name="" id="" class="form-input input-money" placeholder="Harga">
                                                </td>
                                                <td class="total" data-title="Total">
                                                    <input type="text" name="" id="" class="form-input input-money" placeholder="Total" disabled>
                                                </td>
                                                <td class="aksi" data-title="Aksi">
                                                    <button type="button" data-repeater-create class="button button-unstyled"><i class="icon-add"></i> <span class="text-mobile">Tambah</span></button>
                                                    <button type="button" data-repeater-delete class="button button-unstyled"><i class="icon-X"></i> <span class="text-mobile">Hapus</span></button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>  <!--.inner-section-->

                        <div class="inner-section" style="padding-bottom: 20px;margin-bottom: 20px;">
                            <div class="row">
                                <div class="col-12 col-lg-6">
                                    <button type="button" class="button button-secondary button-medium" data-repeater-create> <i class="icon-add"></i> <span>Tambah Baris</span></button>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <table class="table table-nobordered table-invoice-total">
                                        <tbody>
                                            <tr>
                                                <th>
                                                    <strong class="text-highlight">Subtotal</strong>
                                                </th>
                                                <td>
                                                    <input type="text" name="" id="" class="form-input input-money" placeholder="Total" disabled>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    Tambah Diskon
                                                </th>
                                                <td>
                                                    <input type="text" name="" id="" class="form-input input-money" placeholder="Diskon">
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    VAT
                                                </th>
                                                <td>
                                                    <input type="text" name="" id="" class="form-input input-money" placeholder="VAT">
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th><strong class="text-highlight">Total</strong></th>
                                                <td>
                                                    <input type="text" name="" id="" class="form-input total-harga input-money" placeholder="Total" disabled>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>  <!--.inner-section-->

                        <div class="inner-section section-bank-rekening">
                            <div class="row">
                                <div class="col-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="bank-pilihan">Bank</label>
                                        <select name="bank-pilihan" id="bank-pilihan" class="form-input">
                                            <option value="BCA">BCA</option>
                                            <option value="Mandiri">Mandiri</option>
                                            <option value="BNI">BNI</option>
                                            <option value="Jago">Jago</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="nomor-rekening">Nomor Rekening</label>
                                        <input type="text" name="" id="" class="form-input" placeholder="Nomor rekening">
                                    </div>
                                </div>

                                <div class="col-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="catatan">Catatan</label>
                                        <input type="text" name="catatan" id="catatan" class="form-input" placeholder="Catatan">
                                    </div>
                                </div>

                                <div class="col-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="term">Terms</label>
                                        <input type="text" name="term" id="term" class="form-input" placeholder="Terms">
                                    </div>
                                </div>

                            </div>
                        </div>  <!--.inner-section-->



                    </div>
                </section>
    
                <aside id="sidebar" class="col-12 col-lg-3">
                    <div class="side-item side-aksi">
                        <button class="button button-primary button-block">Download</button>
                        <button class="button button-secondary button-block">Preview</button>
                    </div>
    
                    <div class="side-item">
                        <label for="template-invoice">Template</label>
                        <select name="template-invoice" class="form-input" id="template-invoice">
                            <option value="Invoice Template 1">Invoice Template 1</option>
                            <option value="Invoice Template 2">Invoice Template 2</option>
                        </select>
                    </div>
                </aside>
            </div>
        </form>
    </div> <!--.container-->
</main>

<?php require_once('component-footer.php'); ?>