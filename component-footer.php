    </div> <!--#wrapper-->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.2.2/js/swiper.min.js"></script>
    <script src="vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="vendor/repeater/jquery.repeater.min.js"></script>
    <script src="vendor/robinherbots-inputmask/jquery.inputmask.min.js"></script>

    <script>
        const mySwiper = new Swiper('.bank-popular-slider', {
            slidesPerView: 3,
            slidesPerColumn: 2,
            spaceBetween: 30,
            spaceBetween: 16,
            breakpoints: {
                767: {
                    slidesPerView: 1.2,
                    slidesPerColumn: 1,
                    slidesPerGroup :1,
                },
                991: {
                    slidesPerView: 3.2,
                    slidesPerColumn: 1,
                    slidesPerGroup :1,
                    spaceBetween: 16,
                },
            },
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
        });

        //Upload Logo
        const fileInput = document.querySelector('#upload-logo'),
        filePreview = document.querySelector('#select-image .inner'),
        fileWrapper = document.querySelector('#select-image');

        fileInput.addEventListener("change", handleFiles, false);

        function handleFiles(){
            if (this.files.length) {
                filePreview.innerHTML = "";
                const img = document.createElement("img");

                filePreview.appendChild(img);
                img.src = URL.createObjectURL(this.files[0]);
                img.classList.add('img-preview');
                img.onload = function() {
                    URL.revokeObjectURL(this.src);
                }

                fileWrapper.classList.add('preview');
            }
        }

        $(function () {
            $('.datepicker').datepicker({
                format: "dd/mm/yyyy",
                orientation: "bottom auto",
                autoclose: true,
                todayHighlight: true
            });

            $('.repeater').repeater({
                initEmpty: false,
                show: function () {
                    $(this).slideDown();
                },
                defaultValues: {
                    'item-qty': 1,
                },
                hide: function (deleteElement) {
                    $(this).slideUp(deleteElement);
                },
                isFirstItemUndeletable: true
            });

            //https://github.com/RobinHerbots/Inputmask
            $('.input-money').inputmask('numeric', {
                'alias': 'numeric', 
                'groupSeparator': '.', 
                'digits': 0, 
                'digitsOptional': false, 
                'prefix': 'Rp ', 
                'placeholder': '0'
            })
        });



    </script>
</body>
</html>