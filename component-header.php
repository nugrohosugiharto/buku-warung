<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Buku Warung</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.2.2/css/swiper.min.css" />
    <link rel="stylesheet" href="vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet"> 


    <link rel="stylesheet" href="assets/icons/style.css">
    <link rel="stylesheet" href="assets/style.css">

</head>
<body>
    <div id="wrapper">
        <header>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-6 col-logo">
                        <a href="#"><img src="assets/images/BukuWarung-logo.png" alt="Buku Warung"></a>
                    </div> <!--.col-->
                    <div class="col-6 col-menu">
                        <ul class="header-nav">
                            <li class="menu-item notif unread"><a href="#"><i class="icon-notif"></i></a></li>
                            <li class="menu-item user dropdown"><span class="user-initial">HD</span>
                                <ul class="submenu">
                                    <li class="menu-item"><a href="#"> Menu Satu</a></li>
                                    <li class="menu-item"><a href="#"> Menu Dua</a></li>
                                    <li class="menu-item"><a href="#"> Menu Tiga</a></li>
                                    <li class="menu-item"><a href="#"> Menu Empat</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div> <!--.col-->
                </div> <!--.row-->
            </div>
        </header>