<?php require_once('component-header.php'); ?>

<main>
    <div class="container">
        <section class="head-wrap">
            <h1 class="page-title">Daftar Kode Bank</h1>
            <p class="page-subtitle">Daftar lengkap kode semua Bank di Indonesia</p>
        </section>

        <section class="popular-bank">
            <div class="title-wrap">
                <h3 class="title-box">
                    <span class="text-desktop">
                        Popular<br><span class="text-highlight">Bank</span>
                    </span>
                    <span class="text-mobile">
                        Popular <span class="text-highlight">Bank</span>
                    </span>
                </h3>
            </div>
            <div class="banks-wrap">

                <div class="swiper bank-popular-slider">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="bank-box">
                                <div class="logo-bank">
                                    <div class="inner">
                                        <img src="assets/images/bank-bca.png" alt="Bank BCA">
                                    </div>
                                </div>
                                <div class="content-bank">
                                    <h4 class="title-bank">Bank BCA</h4>
                                    <p class="kode-bank">Kode Bank - 014</p>
                                    <button class="btn btn-primary btn-small">Salin Kode Bank</button>
                                </div>
                            </div> <!--.bank-box-->
                        </div>
                        <div class="swiper-slide">
                            <div class="bank-box">
                                <div class="logo-bank">
                                    <div class="inner">
                                        <img src="assets/images/bank-bri.png" alt="Bank BCA">
                                    </div>
                                </div>
                                <div class="content-bank">
                                    <h4 class="title-bank">Bank BRI</h4>
                                    <p class="kode-bank">Kode Bank - 002</p>
                                    <button class="btn btn-primary btn-small">Salin Kode Bank</button>
                                </div>
                            </div> <!--.bank-box-->
                        </div>
                        <div class="swiper-slide">
                            <div class="bank-box">
                                <div class="logo-bank">
                                    <div class="inner">
                                        <img src="assets/images/bank-bni.png" alt="Bank BCA">
                                    </div>
                                </div>
                                <div class="content-bank">
                                    <h4 class="title-bank">Bank BNI</h4>
                                    <p class="kode-bank">Kode Bank - 009</p>
                                    <button class="btn btn-primary btn-small">Salin Kode Bank</button>
                                </div>
                            </div> <!--.bank-box-->
                        </div>
                        <div class="swiper-slide">
                            <div class="bank-box">
                                <div class="logo-bank">
                                    <div class="inner">
                                        <img src="assets/images/bank-jago.png" alt="Bank BCA">
                                    </div>
                                </div>
                                <div class="content-bank">
                                    <h4 class="title-bank">Bank Jago</h4>
                                    <p class="kode-bank">Kode Bank - 542</p>
                                    <button class="btn btn-primary btn-small">Salin Kode Bank</button>
                                </div>
                            </div> <!--.bank-box-->
                        </div>
                        <div class="swiper-slide">
                            <div class="bank-box">
                                <div class="logo-bank">
                                    <div class="inner">
                                        <img src="assets/images/bank-mandiri.png" alt="Bank BCA">
                                    </div>
                                </div>
                                <div class="content-bank">
                                    <h4 class="title-bank">Bank Mandiri</h4>
                                    <p class="kode-bank">Kode Bank - 008</p>
                                    <button class="btn btn-primary btn-small">Salin Kode Bank</button>
                                </div>
                            </div> <!--.bank-box-->
                        </div>
                        <div class="swiper-slide">
                            <div class="bank-box">
                                <div class="logo-bank">
                                    <div class="inner">
                                        <img src="assets/images/bank-btn.png" alt="Bank BCA">
                                    </div>
                                </div>
                                <div class="content-bank">
                                    <h4 class="title-bank">Bank BTN</h4>
                                    <p class="kode-bank">Kode Bank - 200</p>
                                    <button class="btn btn-primary btn-small">Salin Kode Bank</button>
                                </div>
                            </div> <!--.bank-box-->
                        </div>
                        <div class="swiper-slide">
                            <div class="bank-box">
                                <div class="logo-bank">
                                    <div class="inner">
                                        <img src="assets/images/bank-bca.png" alt="Bank BCA">
                                    </div>
                                </div>
                                <div class="content-bank">
                                    <h4 class="title-bank">Bank BCA</h4>
                                    <p class="kode-bank">Kode Bank - 014</p>
                                    <button class="btn btn-primary btn-small">Salin Kode Bank</button>
                                </div>
                            </div> <!--.bank-box-->
                        </div>
                        <div class="swiper-slide">
                            <div class="bank-box">
                                <div class="logo-bank">
                                    <div class="inner">
                                        <img src="assets/images/bank-bca.png" alt="Bank BCA">
                                    </div>
                                </div>
                                <div class="content-bank">
                                    <h4 class="title-bank">Bank BCA</h4>
                                    <p class="kode-bank">Kode Bank - 014</p>
                                    <button class="btn btn-primary btn-small">Salin Kode Bank</button>
                                </div>
                            </div> <!--.bank-box-->
                        </div>
                        <div class="swiper-slide">
                            <div class="bank-box">
                                <div class="logo-bank">
                                    <div class="inner">
                                        <img src="assets/images/bank-bca.png" alt="Bank BCA">
                                    </div>
                                </div>
                                <div class="content-bank">
                                    <h4 class="title-bank">Bank BCA</h4>
                                    <p class="kode-bank">Kode Bank - 014</p>
                                    <button class="btn btn-primary btn-small">Salin Kode Bank</button>
                                </div>
                            </div> <!--.bank-box-->
                        </div>
                        <div class="swiper-slide">
                            <div class="bank-box">
                                <div class="logo-bank">
                                    <div class="inner">
                                        <img src="assets/images/bank-bca.png" alt="Bank BCA">
                                    </div>
                                </div>
                                <div class="content-bank">
                                    <h4 class="title-bank">Bank BCA</h4>
                                    <p class="kode-bank">Kode Bank - 014</p>
                                    <button class="btn btn-primary btn-small">Salin Kode Bank</button>
                                </div>
                            </div> <!--.bank-box-->
                        </div>
                        <div class="swiper-slide">
                            <div class="bank-box">
                                <div class="logo-bank">
                                    <div class="inner">
                                        <img src="assets/images/bank-bca.png" alt="Bank BCA">
                                    </div>
                                </div>
                                <div class="content-bank">
                                    <h4 class="title-bank">Bank BCA</h4>
                                    <p class="kode-bank">Kode Bank - 014</p>
                                    <button class="btn btn-primary btn-small">Salin Kode Bank</button>
                                </div>
                            </div> <!--.bank-box-->
                        </div>
                        <div class="swiper-slide">
                            <div class="bank-box">
                                <div class="logo-bank">
                                    <div class="inner">
                                        <img src="assets/images/bank-bca.png" alt="Bank BCA">
                                    </div>
                                </div>
                                <div class="content-bank">
                                    <h4 class="title-bank">Bank BCA</h4>
                                    <p class="kode-bank">Kode Bank - 014</p>
                                    <button class="btn btn-primary btn-small">Salin Kode Bank</button>
                                </div>
                            </div> <!--.bank-box-->
                        </div>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>

                

            </div> <!--.banks-wrap-->
        </section> <!--.popular-bank-->

        <section class="explore dblock-desktop">
            <div class="title-wrap">
                <h3 class="title-section title-inline">Explore</h3>
                <span class="badge-total">90</span>
            </div>

            <div class="search-wrap">
                <form action="">
                    <div class="form-group">
                        <i class="icon-search"></i>
                        <input type="search" class="form-input input-search" placeholder="Cari kode bank">
                    </div>
                </form>
            </div> <!--.search-wrap-->

            <div class="row">
                <div class="col-12 col-lg-3">
                    <div class="filter-wrap filter-category">
                        <h3 class="filter-title">Kategori Bank</h3>
                        <div class="filter-body">
                            <ul class="categories">
                                <li>
                                    <div class="checkbox-wrap">
                                        <input type="checkbox" name="" id="bank-semua">
                                        
                                        <label for="bank-semua" class="label-wrap">
                                            Semua Bank
                                            <span class="count">90</span>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox-wrap">
                                        <input type="checkbox" name="" id="bank-bumn">

                                        <label for="bank-bumn" class="label-wrap">
                                            Kode Bank BUMN
                                            <span class="count">9</span>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox-wrap">
                                        <input type="checkbox" name="" id="bank-swasta">

                                        <label for="bank-swasta" class="label-wrap">
                                            Kode Bank Swasta
                                            <span class="count">52</span>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox-wrap">
                                        <input type="checkbox" name="" id="bank-daerah">

                                        <label for="bank-daerah" class="label-wrap">
                                            Kode Bank Daerah
                                            <span class="count">29</span>
                                        </label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div> <!--.filter-wrap-->

                    <div class="filter-wrap filter-pilihan-bank">
                        <div class="filter-body">
                            <form action="">
                                <div class="form-group">
                                    <label for="bank-pilihan">Pilihan Bank</label>
                                    <input type="text" name="bank-pilihan" id="bank-pilihan" class="form-input" placeholder="Bank BCA - 014">
                                </div>

                                <div class="form-group">
                                    <label for="bank-rekening">Masukkan Nomor Rekening</label>
                                    <input type="text" name="bank-rekening" id="bank-rekening" class="form-input" placeholder="014 - 23432543631">
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Salin Kode Bank dan Nomor Rekening</button>
                                </div>
                            </form>
                        </div>
                    </div> <!--.filter-wrap-->
                </div> <!--.col-->

                <div class="col-12 col-lg-9">
                    <div class="table-div table-bank">
                        <div class="table-header">
                            <div class="row">
                                <div class="col-5 nama-bank">Nama Bank</div>
                                <div class="col-5 kode-bank">Kode Bank</div>
                                <div class="col-2 aksi"></div>
                            </div>
                        </div>

                        <div class="table-loading"><img src="assets/images/ajax-loader.gif" alt="Loading..."></div>
                        
                        <div class="table-body">
                            <div class="row">
                                <div class="col-5 nama-bank">
                                    <div class="bank-logo"><img src="assets/images/bank-bni.png" alt="Bank BNI"></div>
                                    <div class="bank-teks">Bank BNI</div>
                                </div>
                                <div class="col-5 kode-bank">009</div>
                                <div class="col-2 aksi"><button class="btn-action btn-copy"><i class="icon-copy"></i></button></div>
                            </div>
                            <div class="row">
                                <div class="col-5 nama-bank">
                                    <div class="bank-logo"><img src="assets/images/bank-bca.png" alt="Bank BCA"></div>
                                    <div class="bank-teks">Bank BCA</div>
                                </div>
                                <div class="col-5 kode-bank">014</div>
                                <div class="col-2 aksi"><button class="btn-action btn-copy"><i class="icon-copy"></i></button></div>
                            </div>
                            <div class="row">
                                <div class="col-5 nama-bank">
                                    <div class="bank-logo"><img src="assets/images/bank-mandiri.png" alt="Bank Mandiri"></div>
                                    <div class="bank-teks">Bank Mandiri</div>
                                </div>
                                <div class="col-5 kode-bank">008</div>
                                <div class="col-2 aksi"><button class="btn-action btn-copy"><i class="icon-copy"></i></button></div>
                            </div>
                            <div class="row">
                                <div class="col-5 nama-bank">
                                    <div class="bank-logo"><img src="assets/images/bank-bri.png" alt="Bank BRI"></div>
                                    <div class="bank-teks">Bank BRI</div>
                                </div>
                                <div class="col-5 kode-bank">002</div>
                                <div class="col-2 aksi"><button class="btn-action btn-copy"><i class="icon-copy"></i></button></div>
                            </div>
                            <div class="row">
                                <div class="col-5 nama-bank">
                                    <div class="bank-logo"><img src="assets/images/bank-jago.png" alt="Bank Jago"></div>
                                    <div class="bank-teks">Bank Jago</div>
                                </div>
                                <div class="col-5 kode-bank">542</div>
                                <div class="col-2 aksi"><button class="btn-action btn-copy"><i class="icon-copy"></i></button></div>
                            </div>
                            <div class="row">
                                <div class="col-5 nama-bank">
                                    <div class="bank-logo"><img src="assets/images/bank-btn.png" alt="Bank BTN"></div>
                                    <div class="bank-teks">Bank BTN</div>
                                </div>
                                <div class="col-5 kode-bank">200</div>
                                <div class="col-2 aksi"><button class="btn-action btn-copy"><i class="icon-copy"></i></button></div>
                            </div>
                        </div>
                    </div> <!--.table-div-->
                </div> <!--.col-->
            </div> <!--.row-->
        </section>

        <section class="explore dblock-mobile">
            <div class="row">
                <div class="col-12">
                    <div class="filter-wrap filter-bank-pilihan">
                        <div class="filter-body">
                            <form action="">
                                <div class="form-group">
                                    <label for="bank-pilihan">Pilihan Bank</label>
                                    <input type="text" name="bank-pilihan" id="bank-pilihan" class="form-input" placeholder="Bank BCA - 014">
                                </div>

                                <div class="form-group">
                                    <label for="bank-rekening">Masukkan Nomor Rekening</label>
                                    <input type="text" name="bank-rekening" id="bank-rekening" class="form-input" placeholder="014 - 23432543631">
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Salin Kode Bank dan Nomor Rekening</button>
                                </div>
                            </form>
                        </div>
                    </div> <!--.filter-wrap-->
                </div>

                <div class="col-12">
                    <div class="bank-wrapper">
                        <div class="title-wrap">
                            <h3 class="title-section title-inline">Explore</h3>
                            <span class="badge-total">90</span>
                        </div>

                        <div class="search-wrap">
                            <form action="">
                                <div class="form-group">
                                    <i class="icon-search"></i>
                                    <input type="search" class="form-input input-search" placeholder="Cari kode bank">
                                </div>
                            </form>
                        </div> <!--.search-wrap-->

                        <div class="filter-wrap filter-category filter-category-mobile">
                            <h3 class="filter-title">Kategori Bank</h3>
                            <div class="filter-body">
                                <ul class="categories">
                                    <li>
                                        <div class="checkbox-wrap">
                                            <input type="checkbox" name="" id="bank-semua-mobile">
                                            
                                            <label for="bank-semua-mobile" class="label-wrap">
                                                Semua Bank
                                                <span class="count">(90)</span>
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="checkbox-wrap">
                                            <input type="checkbox" name="" id="bank-bumn-mobile">

                                            <label for="bank-bumn-mobile" class="label-wrap">
                                                Kode Bank BUMN
                                                <span class="count">(9)</span>
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="checkbox-wrap">
                                            <input type="checkbox" name="" id="bank-swasta-mobile">

                                            <label for="bank-swasta-mobile" class="label-wrap">
                                                Kode Bank Swasta
                                                <span class="count">(52)</span>
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="checkbox-wrap">
                                            <input type="checkbox" name="" id="bank-daerah-mobile">

                                            <label for="bank-daerah-mobile" class="label-wrap">
                                                Kode Bank Daerah
                                                <span class="count">(29)</span>
                                            </label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div> <!--.filter-wrap-->

                        <div class="table-div table-bank">
                            <div class="table-loading"><img src="assets/images/ajax-loader.gif" alt="Loading..."></div>
                            <div class="table-body">
                                <div class="row">
                                    <div class="col-9 nama-bank">
                                        <div class="bank-logo"><img src="assets/images/bank-bni.png" alt="Bank BNI"></div>
                                        <div class="bank-teks">Bank BNI - 009</div>
                                    </div>
                                    <div class="col-3 aksi"><button class="btn-action btn-copy"><i class="icon-copy"></i></button></div>
                                </div>
                                <div class="row">
                                    <div class="col-9 nama-bank">
                                        <div class="bank-logo"><img src="assets/images/bank-bca.png" alt="Bank BCA"></div>
                                        <div class="bank-teks">Bank BCA - 014</div>
                                    </div>
                                    <div class="col-3 aksi"><button class="btn-action btn-copy"><i class="icon-copy"></i></button></div>
                                </div>
                                <div class="row">
                                    <div class="col-9 nama-bank">
                                        <div class="bank-logo"><img src="assets/images/bank-mandiri.png" alt="Bank Mandiri"></div>
                                        <div class="bank-teks">Bank Mandiri - 008</div>
                                    </div>
                                    <div class="col-3 aksi"><button class="btn-action btn-copy"><i class="icon-copy"></i></button></div>
                                </div>
                                <div class="row">
                                    <div class="col-9 nama-bank">
                                        <div class="bank-logo"><img src="assets/images/bank-bri.png" alt="Bank BRI"></div>
                                        <div class="bank-teks">Bank BRI - 002</div>
                                    </div>
                                    <div class="col-3 aksi"><button class="btn-action btn-copy"><i class="icon-copy"></i></button></div>
                                </div>
                                <div class="row">
                                    <div class="col-9 nama-bank">
                                        <div class="bank-logo"><img src="assets/images/bank-jago.png" alt="Bank Jago"></div>
                                        <div class="bank-teks">Bank Jago - 542</div>
                                    </div>
                                    <div class="col-3 aksi"><button class="btn-action btn-copy"><i class="icon-copy"></i></button></div>
                                </div>
                                <div class="row">
                                    <div class="col-9 nama-bank">
                                        <div class="bank-logo"><img src="assets/images/bank-btn.png" alt="Bank BTN"></div>
                                        <div class="bank-teks">Bank BTN - 200</div>
                                    </div>
                                    <div class="col-3 aksi"><button class="btn-action btn-copy"><i class="icon-copy"></i></button></div>
                                </div>
                            </div>
                        </div> <!--.table-div-->

                    </div>
                </div>
            </div> <!--.row-->
        </section>
    </div> <!--.container-->

</main>
<?php require_once('component-footer.php'); ?>